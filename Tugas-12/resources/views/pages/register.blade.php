<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook | Register</title>
</head>

<body>
    <h1>
        <pre>Buat Account Baru</pre>
    </h1>
    <h2>
        <pre>Sign Up Form</pre>
    </h2>
    <form action="/welcome" method="post">
        @csrf
        <pre>
            <label>First Name:</label>
            <input type="text" name="fname">
        </pre>
        <pre>
            <label>Last Name:</label>
            <input type="text" name="lname">
        </pre>
        <pre>
            <label>Gender:</label>
            <input type="radio" name="gender" value="1"> Man
            <input type="radio" name="gender" value="2"> Woman
            <input type="radio" name="gender" value="3"> Other
        </pre>
        <pre>
            <Label>Nationality:</Label>
            <select name="nationality">
                <option value="id">Indonesia</option>
                <option value="sg">Singapura</option>
            </select>
        </pre>
        <pre>
            <label>Language Spoken:</label>
            <input type="checkbox" name="id"> Bahasa Indonesia
            <input type="checkbox" name="en"> English
            <input type="checkbox" name="ar"> Arabic
            <input type="checkbox" name="jp"> Japanese
        </pre>
        <pre>
            <label>Bio:</label>
            <textarea name="bio" cols="30" rows="10"></textarea>
        </pre>
        <pre>
            <input type="submit" value="Sign Up">
        </pre>
    </form>
</body>

</html>