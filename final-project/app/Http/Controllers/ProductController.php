<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $product = Product::get();
        return view('pages.product', ['product' => $product]);
    }

    public function index()
    {
        $product = Product::get();
        return view('pages.table', ['product' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'stock' => 'required',
            'price' => 'required',
            'category' => 'required',
            'image_url' => 'required',
            'description' => 'required'
        ]);

        $create = new Product;

        $create->name = $request->name;
        $create->stock = $request->stock;
        $create->price = $request->price;
        $create->discount_price = $request->discount_price;
        $create->category = $request->category;
        $create->image_url = $request->image_url;
        $create->description = $request->description;

        $create->save();

        return redirect('/table');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Product::find($id);
        return view('components.detail', ['detail' => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $update = Product::find($id);
        return view('components.update', ['update' => $update]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'stock' => 'required',
            'price' => 'required',
            'category' => 'required',
            'image_url' => 'required',
            'description' => 'required'
        ]);

        $update = Product::find($id);

        $update->name = $request['name'];
        $update->stock = $request['stock'];
        $update->price = $request['price'];
        $update->discount_price = $request['discount_price'];
        $update->category = $request['category'];
        $update->image_url = $request['image_url'];
        $update->description = $request['description'];

        $update->save();

        return redirect('/table');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Product::find($id);

        $delete->delete();

        return redirect('/table');
    }
}
