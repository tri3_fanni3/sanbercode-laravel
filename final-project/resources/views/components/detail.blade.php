@extends('layout.master')

@section('title')
Detail Product
@endsection

@section('topnav')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Detail Product</li>
</ol>
@endsection

@section('content')
<div class="card m-0" style="max-width:100%; border-radius:12px">
    <div class="row g-0">
        <div class="col-md-6">
            <img src="{{$detail -> image_url}}" class="img-fluid rounded-start" style="object-fit:cover; border-radius:12px;" alt="image_url">
        </div>
        <div class="col-md-6">
            <div class="card-body">
                <h3 class="card-text text-bold">{{$detail -> name}}</h3>
                <p class="card-text text-secondary">{{$detail -> category}}</p>
                @if ($detail -> discount_price !== null)
                <h5 class="card-text" style="text-decoration:line-through;">{{$detail -> discount_price}}</h5>
                @endif
                <h2 class="card-text text-danger">{{$detail -> price}}</h2>
                <p class="card-text text-body-emphasis">{{$detail -> description}}</p>
                <p class="card-text text-primary">Stock {{$detail -> stock}}</p>
            </div>
        </div>
    </div>
</div>
@endsection