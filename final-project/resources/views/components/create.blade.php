@extends('layout.master')

@section('title')
Create Product
@endsection

@section('topnav')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/table">Table</a></li>
    <li class="breadcrumb-item active">Create Product</li>
</ol>
@endsection

@section('content')
<form action="/product" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Nama Barang</label>
                        <input type="text" name="name" placeholder="input nama barang" class="form-control">
                    </div>
                </div>
                @error('name')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Stok Barang</label>
                        <input type="text" name="stock" placeholder="input stok barang" class="form-control">
                    </div>
                </div>
                @error('stock')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Harga Barang</label>
                        <input type="text" name="price" placeholder="input harga barang" class="form-control">
                    </div>
                </div>
                @error('price')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Harga Diskon</label>
                        <input type="text" name="discount_price" placeholder="input diskon barang" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Jenis Barang</label>
                        <select name="category" class="form-control">
                            <option value="">--Pilih Kategori--</option>
                            <option value="Teknologi">Teknologi</option>
                            <option value="Makanan">Makanan</option>
                            <option value="Minuman">Minuman</option>
                            <option value="Hiburan">Hiburan</option>
                            <option value="Kendaraan">Kendaraan</option>
                        </select>
                    </div>
                </div>
                @error('category')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Gambar Barang</label>
                        <input type="text" name="image_url" placeholder="input image url" class="form-control">
                    </div>
                </div>
                @error('image_url')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
        </div>

        <div class="row form-group">
            <div class="col">
                <label>Deskripsi</label>
                <textarea name="description" placeholder="input deskripsi" class="form-control" rows="4"></textarea>
            </div>
        </div>
        @error('description')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>

    <div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/table">
            <button type="button" class="btn btn-secondary">Cancel</button>
        </a>
    </div>
</form>
@endsection