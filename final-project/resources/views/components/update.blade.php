@extends('layout.master')

@section('title')
Update Product
@endsection

@section('topnav')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/table">Table</a></li>
    <li class="breadcrumb-item active">Update Product</li>
</ol>
@endsection

@section('content')
<form action="/product/{{$update -> id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Nama Barang</label>
                        <input type="text" name="name" value="{{$update -> name}}" placeholder="input nama barang" class="form-control">
                    </div>
                </div>
                @error('name')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Stok Barang</label>
                        <input type="text" name="stock" value="{{$update -> stock}}" placeholder="input stok barang" class="form-control">
                    </div>
                </div>
                @error('stock')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Harga Barang</label>
                        <input type="text" name="price" value="{{$update -> price}}" placeholder="input harga barang" class="form-control">
                    </div>
                </div>
                @error('price')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Harga Diskon</label>
                        <input type="text" name="discount_price" value="{{$update -> discount_price}}" placeholder="input diskon barang" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Jenis Barang</label>
                        <select name="category" class="form-control">
                            @if($update -> category === "Teknologi")
                            <option value="Teknologi" selected>Teknologi</option>
                            <option value="Makanan">Makanan</option>
                            <option value="Minuman">Minuman</option>
                            <option value="Hiburan">Hiburan</option>
                            <option value="Kendaraan">Kendaraan</option>

                            @elseif($update -> category === "Makanan")
                            <option value="Teknologi">Teknologi</option>
                            <option value="Makanan" selected>Makanan</option>
                            <option value="Minuman">Minuman</option>
                            <option value="Hiburan">Hiburan</option>
                            <option value="Kendaraan">Kendaraan</option>

                            @elseif($update -> category === "Minuman")
                            <option value="Teknologi">Teknologi</option>
                            <option value="Makanan">Makanan</option>
                            <option value="Minuman" selected>Minuman</option>
                            <option value="Hiburan">Hiburan</option>
                            <option value="Kendaraan">Kendaraan</option>

                            @elseif($update -> category === "Hiburan")
                            <option value="Teknologi">Teknologi</option>
                            <option value="Makanan">Makanan</option>
                            <option value="Minuman">Minuman</option>
                            <option value="Hiburan" selected>Hiburan</option>
                            <option value="Kendaraan">Kendaraan</option>

                            @elseif($update -> category === "Kendaraan")
                            <option value="Teknologi">Teknologi</option>
                            <option value="Makanan">Makanan</option>
                            <option value="Minuman">Minuman</option>
                            <option value="Hiburan">Hiburan</option>
                            <option value="Kendaraan" selected>Kendaraan</option>

                            @else
                            <option value="">--Pilih Kategori--</option>

                            @endif
                        </select>
                    </div>
                </div>
                @error('category')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="col">
                <div class="row form-group">
                    <div class="col">
                        <label>Gambar Barang</label>
                        <input type="text" name="image_url" value="{{$update -> image_url}}" placeholder="input image url" class="form-control">
                    </div>
                </div>
                @error('image_url')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
        </div>

        <div class="row form-group">
            <div class="col">
                <label>Deskripsi</label>
                <textarea name="description" placeholder="input deskripsi" class="form-control" rows="4">{{$update -> description}}</textarea>
            </div>
        </div>
        @error('description')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>

    <div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/table">
            <button type="button" class="btn btn-secondary">Cancel</button>
        </a>
    </div>
</form>
@endsection