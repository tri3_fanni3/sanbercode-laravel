@extends('layout.master')

@section('title')
Catalog Products
@endsection

@section('content')
<div class="row row-cols-1 row-cols-md-4 g-4 m-0">
    @foreach ($product as $item)
    <div class="col p-2" style="height:380px;">
        <div class="card h-100" style="overflow:hidden; border-radius:10px;">
            <img src="{{$item -> image_url}}" class="card-img-top" style="width:100%; height:50%; object-fit:cover;" alt="image_url">
            <div class="card-body">
                <h5 class="card-title mb-1 text-bold" style="overflow:hidden; text-overflow:ellipsis; display:-webkit-box; -webkit-line-clamp:2; -webkit-box-orient:vertical;">{{$item -> name}}</h5>
                @if ($item -> discount_price !== null)
                <p class="card-text mb-1 text-secondary" style="text-decoration:line-through;">{{$item -> discount_price}}</p>
                @endif
                <h4 class="card-text mb-1 text-danger">{{$item -> price}}</h4>
                <h7 class="card-text mb-1 text-primary">Stock {{$item -> stock}}</h7>
            </div>
            <a href="/product/{{$item -> id}}">
                <button class="btn btn-primary btn-block btn-sm" style="border-radius:0;">Detail Product</button>
            </a>
        </div>
    </div>
    @endforeach
</div>
@endsection