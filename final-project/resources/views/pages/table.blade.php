@extends('layout.master')

@section('title')
Table Products
@endsection

@push('pagination')
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
<script src="https://cdn.datatables.net/v/bs4/dt-1.13.8/datatables.min.js"></script>
@endpush

@push('style')
<link href="https://cdn.datatables.net/v/bs4/dt-1.13.8/datatables.min.css" rel="stylesheet">
@endpush

@section('content')
<div class="card-header">
    <a href="/product/create" class="btn btn-primary">Create Product+</a>
</div>
<div class="card-body table-responsive m-0" style="height:720px;">
    <table class="table table-hover table-bordered text-nowrap" id="myTable">
        <thead class="table-head-fixed">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Harga</th>
                <th scope="col">Harga Diskon</th>
                <th scope="col">Kategori</th>
                <th scope="col">Gambar</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($product as $key => $value)
            <tr>
                <th scope="row">{{$key + 1}}</th>
                <td>{{$value -> name}}</td>
                <td>{{$value -> price}}</td>
                <td>{{$value -> discount_price}}</td>
                <td>{{$value -> category}}</td>
                <td style="overflow:hidden; width:225px;">
                    <img src="{{$value -> image_url}}" style="width:100%; height:125px; object-fit:cover; border-radius:5px" alt="image_url">
                </td>
                <td>
                    <div style="display:flex; justify-content:center; gap:10px;">
                        <a href=" /product/{{$value -> id}}/edit" class="btn btn-info btn-sm">Update</a>
                        <form action="/product/{{$value -> id}}" method="POST">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection