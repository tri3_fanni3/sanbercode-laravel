<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// php artisan route:list
Route::resource('product', ProductController::class);

// List Product
Route::get('/', [ProductController::class, 'home']);
Route::get('/table', [ProductController::class, 'index']);

// // Create Product
// Route::get('/create-product', [ProductController::class, 'create']);
// Route::post('/create-product', [ProductController::class, 'store']);

// // Detail Product
// Route::get('/detail-product/{id}', [ProductController::class, 'show']);

// // Update Product
// Route::get('/product/{id}/edit', [ProductController::class, 'edit']);
// Route::put('/product/{id}/update', [ProductController::class, 'update']);

// // Delete Product
// Route::delete('/product/{id}', [ProductController::class, 'destroy']);
