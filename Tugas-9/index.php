<?php
require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("shaun");
echo "name : $sheep->name <br>";
echo "legs : $sheep->legs <br>";
echo "cold_blonded : $sheep->cold_blooded <br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "name : $sungokong->name <br>";
echo "legs : $sungokong->legs <br>";
echo "cold_blonded : $sungokong->cold_blooded <br>";
echo "yell : ";
$sungokong->yell(); // "Auooo"
echo "<br>";

echo "<br>";
$kodok = new Frog("buduk");
echo "name : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold_blonded : $kodok->cold_blooded <br>";
echo "jump : ";
$kodok->jump() ; // "hop hop"
?>